def transform(obj):
    """ Function for transform obj to tree list

    Input:  string - some string

    Return: pointers - list with pointers, like 'int' or 'str'
            int_list -  list with sorted all int from string
            str_list -  list with sorted all str from string
    """
    if type(obj) is str:
        pointers = obj.split(' ')
    else:    
        pointers = obj[:]

    int_list = []
    str_list = []

    for index, element in enumerate(pointers):
        try:
            new_int = int(element)
            int_list.append(new_int)
            pointers[index] = 'int'
        except ValueError:
            str_list.append(element)
            pointers[index] = 'str'

    return (pointers, sorted(int_list), sorted(str_list)) 


def main(element):
    """ Main Function

    Input: element - some element (list or string)
    """
    data = transform(element)  

    main_list = data[0]
    int_list = data[1]
    str_list = data[2]

    for index, element in enumerate(main_list):
        if element == 'str':
            main_list[index] = str_list[0]
            str_list.pop(0)
        elif element == 'int':
            main_list[index] = int_list[0]
            int_list.pop(0)

    print(main_list)
    return main_list


test_string = 'yellow white 2 5 green red 6 1'
test_list = ['yellow', 'white', 2, 5, 'green', 'red', 6, 1] 

if __name__ == '__main__':
    main(test_list)
    main(test_string)
