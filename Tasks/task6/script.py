def count(recipe, available):
    """ Functon for count available portions from recipe 
    
    Input:  recipe - dictionari with recipe of products
            avalible - dictionary with avalible products

    Return: count - count of avalible portions        
    """
    # set with cont of avalible portions
    avalible_set = set()

    for key, item in recipe.items():
        needed_count = item
        have_count = available.get(key)
        
        if have_count:
            avalible_count = have_count // needed_count
        else:
            avalible_count = 0

        avalible_set.add(avalible_count)
    
    count = min(avalible_set)
    print(count)
    return count


recipe1 = {"flour": 500, "sugar": 200, "eggs": 1}
available1 = {"flour": 1200, "sugar": 1200, "eggs": 5, "milk": 200}

recipe2 = {"flour": 500, "sugar": 200, "eggs": 1}
available2 = {"sugar": 1200, "eggs": 5, "milk": 200}


if __name__ == '__main__':
    count(recipe1, available1) # --> 2
    count(recipe2, available2) # --> 0
