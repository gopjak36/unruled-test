import itertools

def search_max_number(l):
    """ Fuction for seacrh max number from list int
    
    Input: l - list with int numbers

    Return: max_number - max number from permutations
    """
    # get all permutations as int number, PEP8 sorry me :)
    permutations = [int(''.join(str(num) for num in permutation)) for permutation in itertools.permutations(l)]

    max_number = max(permutations)

    print(max_number)
    return max_number

list1 = [70, 8, 20, 1, 13]      # --> 87020131
list2 = [89, 8, 892, 1, 103]    # --> 8989281103

if __name__ == '__main__':
    search_max_number(list1)
    search_max_number(list2)
