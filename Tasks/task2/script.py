def check_list_type(l):
    """ Fcuntion for check list type odd or even

    Input: l - list with int number

    Return: l_type - type of list
    """
    odd = 0
    even = 0

    for i in l:
        if i % 2 == 0:
            even += 1
        elif i % 2 != 0:
            odd += 1
        else:
            pass 

    if odd > even:
        return 'odd'
    elif even > odd:
        return 'even'
    else:
        return None         

def search_number(l):
    """ Function for search one number in list
        list with all odd or even numbers, except one

    Input: l - list with int numbrs

    Retun: number - odd or even number
    """
    l_type = check_list_type(l[0:3])

    if l_type == 'odd':
        number = [i for i in l if i % 2 == 0]
    elif l_type == 'even':
        number = [i for i in l if i % 2 != 0]
    else:
        number = None

    print(number)
    return number

one_odd = [0, 8, 2, 50, 13, 6, 34]  # --> 13
one_even = [1, 3, 4, 7, 5]          # --> 4
none_list = []                      # --> None

if __name__ == '__main__':
    search_number(one_odd)
    search_number(one_even)
    search_number(none_list)