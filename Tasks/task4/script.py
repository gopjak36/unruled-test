def return_string(l):
    """ Fucntion for return string with names from dictionaries

    Input: l - list with dictionaries

    Return: string - string with names from dictioniries 
    """
    # get all names:
    names = [d.get('name') for d in l]
    names_len = len(names)

    string = ''

    for index, name in enumerate(names):
        if index == 0:
            string = name
        elif index == names_len - 1:
            string += ' & ' + name
        else:
            string += ', ' + name  

    print(string)
    return string


list1 = [{'name': 'John'}, {'name': 'Jack'}, {'name': 'Joe'}] # --> 'John, Jack & Joe'
list2 = [{'name': 'John'}, {'name': 'Jack'}] # --> 'John & Jack'
list3 = [{'name': 'John'}]  # --> 'John'
list4 = [{'name': 'name1'}, {'name': 'name2'}, 
         {'name': 'name3'}, {'name': 'name4'},
         {'name': 'name5'}] # --> 'name1, name2, name3, name4 & name5'
         

if __name__ == '__main__':
    return_string(list1)
    return_string(list2)
    return_string(list3)
    return_string(list4)