import re

def to_camel_case(string):
    """ Function to Transform string to camel case

    Input: string - some string

    Return: camle_string - camel case string
    """
    # split string by ignore chars:
    split_string = re.split('-|_', string)
    
    camel_string = ''

    for index, word in enumerate(split_string):
        if index == 0:
            camel_string += word
        else:
            if word[0].isupper():
                camel_string += word
            else:
                camel_string += word.title()

    print(camel_string)
    return camel_string               


string1 = 'the_phantom_menace'  # --> thePhantomMenace
string2 = 'The-Phantom-Menace'  # --> ThePhantomMenace
string3 ='The-Phantom_Menace'   # --> ThePhantomMenace

if __name__ == '__main__':
    to_camel_case(string1)
    to_camel_case(string2)
    to_camel_case(string3)