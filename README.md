# Unruled tast

## Requirements:

- Python 3.6.3

## Install (local):

1. Clone this repositories:
```
    git clone https://bitbucket.org/gopjak36/unruled-test
```

2. Create virtualenv:
```
    virtualenv -p python3.6 env
```

3. Activate virtualenv:
```
  source env/bin/activate
```

4. And Test.